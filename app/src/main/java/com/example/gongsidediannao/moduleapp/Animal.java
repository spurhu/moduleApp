package com.example.gongsidediannao.moduleapp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @作者 Huyahui
 * @主要功能
 * @创建日期 2019/8/26
 */

public class Animal implements Parcelable {
    private String color;
    private String name;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(color);
        dest.writeString(name);
    }


    public  static final Parcelable.Creator<Animal> CREATOR
            =new Parcelable.Creator<Animal>(){
        public Animal createFromParcel(Parcel in){
            return new Animal(in);
        }
        public Animal[] newArray(int size){
            return new Animal[size];
        }
    };

    protected Animal(Parcel parcel) {
        color=parcel.readString();
        name=parcel.readString();
    }

    public Animal(String color, String name) {
        this.color = color;
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
