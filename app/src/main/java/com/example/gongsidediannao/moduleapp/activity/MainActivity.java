package com.example.gongsidediannao.moduleapp.activity;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.Instrumentation;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.app_base.utils.ToastUtils;
import com.example.gongsidediannao.moduleapp.Constance;
import com.example.gongsidediannao.moduleapp.R;
import com.example.gongsidediannao.moduleapp.utils.SharedPreferenceUtil;
import com.example.gongsidediannao.moduleapp.WebService;
import com.example.gongsidediannao.moduleapp.base.BaseActivity;
import com.example.gongsidediannao.moduleapp.entity.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.example.gongsidediannao.moduleapp.Constance.ACTIVITY_AROUTER;
import static com.example.gongsidediannao.moduleapp.utils.SharedPreferenceUtil.SP_KEY_NAME;
import static com.example.gongsidediannao.moduleapp.WebService.ACTION_START_WEB_SERVICE;

@Route(path = Constance.ACTIVITY_MAIN)
public class MainActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    public static final String KEY_NAME = "name";

    private Button btn_up, btn_down, btn_save;
    private Switch aSwitch;

    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      /*  if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }*/


//        initView();
//        initData();
//
//        Intent intentWeb=new Intent(this, WebService.class);
//        intent.setAction(ACTION_START_WEB_SERVICE);
//        startService(intentWeb);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, TestActivity.class);
        startActivity(intent);

    }

    private void sendBroadToApp() {
        Intent intent = new Intent("com.example.broadcasttest1.MY_BROADCAST");
        intent.setComponent(new ComponentName("com.android.tv.launcherapp", "com.android.tv.launcherapp.MyReceiver"));
        intent.putExtra("name", "支付宝");
        sendBroadcast(intent);
    }


    /**
     * sp数据保存：/data/data//shared_prefs/packageName/**.xml
     */
    private void initData() {
        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    private void initView() {
        aSwitch = (Switch) findViewById(R.id.switch_check);
        btn_up = (Button) findViewById(R.id.btn_up);
        btn_down = (Button) findViewById(R.id.btn_down);
        btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        btn_down.setOnClickListener(this);
        btn_up.setOnClickListener(this);

        btn_save.setText("发送事件");
//        btn_up.setText("testactivity");
        btn_up.setText("发广播");
        btn_down.setText("Arouter");
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(TAG, "onMessageEvent: Mainactivity  " + event.getTimeStamp());
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_up:
                String name = SharedPreferenceUtil.getInstance().getValue(SP_KEY_NAME, "默认小胖");
//                btn_up.setText(name);
                // ARouter.getInstance().build(ACTIVITY_TEST).navigation();
                sendBroadToApp();

                Log.d(TAG, "onClick: btn_up");
                break;
            case R.id.btn_down:
                Log.d(TAG, "onClick: 清空数据");
                ARouter.getInstance().build(ACTIVITY_AROUTER).navigation();
                break;
            case R.id.btn_save:
                Long time = System.currentTimeMillis();
                EventBus.getDefault().post(new MessageEvent(String.valueOf(time)));
                SharedPreferenceUtil.getInstance().putValue(SP_KEY_NAME, "肥仔");
                break;
        }
    }


    public void sendKeyCode(View view) {
        Log.d(TAG, "sendKeyCode: ");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "run KeyEvent: ");
                Instrumentation inst = new Instrumentation();
                inst.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
            }
        }).start();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown: " + keyCode);
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyUp: " + keyCode);
        return super.onKeyUp(keyCode, event);
    }

    int currentP = 0;

    public void arrayPrint(boolean flag, int[] array) {
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; i++) {
                if (flag) { //true up
                    if (currentP > 0) {
                        currentP = currentP - 1;
                        Log.d(TAG, "arrayPrint: 输出元素" + array[currentP]);
                        break;
                    } else if (currentP == 0) {
                        currentP = array.length - 1;
                        Log.d(TAG, "arrayPrint 元素: " + array[currentP]);
                        break;
                    }
                } else {
                    if (currentP == array.length - 1) {
                        currentP = 0;
                        Log.d(TAG, "arrayPrint: 输出元素2**" + array[currentP]);
                    } else {
                        currentP = currentP + 1;
                        Log.d(TAG, "arrayPrint: 输出元素2==" + array[currentP]);
                    }
                    break;
                }
            }

        }
    }

    /**
     * 跳转
     */
    public void go(View view) {
        Uri uri = Uri.parse(Constance.ACTIVITY_AROUTER); // 使用uri跳转
//        ARouter.getInstance().build(Constance.ACTIVITY_AROUTER)
//                .withString("content", "分享数据到微博")
//                .withString("name", "我是胡亚辉 你好")
//                .withParcelable("dog", new Animal("黄色", "小狗金毛"))
//                .navigation();

//        ARouter.getInstance().build(Constance.ACTIVITY_AROUTER).navigation();
        //FactoryDialogFragment.getInstance().show(getSupportFragmentManager(),"FactoryDialogFragment");
    }


    /**
     * fragment
     *
     * @param view
     */
    public void goFragment(View view) {
        Fragment fragment = (Fragment) ARouter.getInstance().build(Constance.FRAGMENT_BALL).navigation();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3) {
            ToastUtils.showToast(this, "传值啦");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
       /* if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }*/
    }
}
