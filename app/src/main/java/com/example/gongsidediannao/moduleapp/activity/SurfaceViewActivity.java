package com.example.gongsidediannao.moduleapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import com.example.app_base.utils.ToastUtils;
import com.example.gongsidediannao.moduleapp.R;
import java.io.File;
import java.io.IOException;

/**
 * @Description: 参考 https://www.cnblogs.com/plokmju/p/android_SurfaceView.html
 * https://blog.csdn.net/u010126792/article/details/86249399
 * https://blog.csdn.net/u014365133/article/details/53330776
 * @Author: huyahui
 * @Date: 2021/10/29 14:21
 */


public class SurfaceViewActivity extends AppCompatActivity {

    private static final String TAG = "SurfaceViewActivity";
    private Button btnPLay, btnPause,btnStop;
    private MediaPlayer mediaPlayer;
    private SurfaceView surfaceView;
    private SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surface_view);

        initView();

        Log.d(TAG, "onCreate: " + Environment.getExternalStorageDirectory().toString());

    }


    private void initView() {
        btnPLay = (Button) findViewById(R.id.btn_play);
        btnPause = (Button) findViewById(R.id.btn_pause);
        btnStop = (Button) findViewById(R.id.btn_stop);
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceView.getHolder().setKeepScreenOn(true);
        btnPLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playView();
            }
        });


        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pauseVideo();
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.stop();
                mediaPlayer.release();
                isPlaying=false;
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.d(TAG, "onProgressChanged 进度: " + seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private boolean isPlaying = false; // 正在播放

    /**
     * 开始播放
     */
    private void playView() {
        Log.d(TAG, "playView isPlaying : " + isPlaying);
        if (isPlaying) {
            return;
        }

        //没有权限问题 https://www.jianshu.com/p/d349047e6515
        String path = "/sdcard/night.mp4";
        File file = new File(path);
        Log.d(TAG, "playView: " + file.getAbsolutePath());
        Log.d(TAG, "playView: 视频资源是否存在 " + file.exists());
        if (!file.exists()) {
            ToastUtils.showToast(this, "视频文件路径错误");
            return;
        }
        mediaPlayer = new MediaPlayer();//MediaPlayer.create(this,Uri.parse(file.getAbsolutePath()));
        mediaPlayer.reset();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(file.getAbsolutePath());
            mediaPlayer.setDisplay(surfaceView.getHolder());
            // mediaPlayer.prepareAsync();//开始加载
            mediaPlayer.prepare();
        } catch (IOException e) {
            Log.d(TAG, "playView: e " + e.getCause());
            e.printStackTrace();
        }

        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                Log.d(TAG, "onPrepared: ");
                mediaPlayer.start();
                mediaPlayer.seekTo(0);
                seekBar.setMax(mediaPlayer.getDuration());
                new Thread() {
                    @Override
                    public void run() {
                        isPlaying = true;
                        while (isPlaying) {
                            int current = mediaPlayer.getCurrentPosition();
                            seekBar.setProgress(current);
                        }
                    }
                }.start();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                isPlaying = false;
                mediaPlayer.release();
                Log.d(TAG, "onCompletion: ");
                ToastUtils.showToast(SurfaceViewActivity.this, "播放完毕");
            }
        });
    }


    /**
     * 暂停播放
     */
    private void pauseVideo() {
        Log.d(TAG, "pauseVideo isPlaying: " + isPlaying);
        if (isPlaying) {
            mediaPlayer.pause();
            isPlaying = !isPlaying;
            btnPause.setText("重新播放");
        } else {
            mediaPlayer.start();
            isPlaying = !isPlaying;
            btnPause.setText("暂停");
            Log.d(TAG, "pauseVideo 暂停: "+isPlaying);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    Log.d(TAG, "onPrepared: ");

                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition());
                    seekBar.setMax(mediaPlayer.getDuration());
                    new Thread() {
                        @Override
                        public void run() {

                                int current = mediaPlayer.getCurrentPosition();
                                seekBar.setProgress(current);
                        }
                    }.start();
                }
            });

        }

    }

}