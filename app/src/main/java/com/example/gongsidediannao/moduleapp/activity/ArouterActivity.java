package com.example.gongsidediannao.moduleapp.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.app_base.utils.ToastUtils;
import com.example.gongsidediannao.moduleapp.Animal;
import com.example.gongsidediannao.moduleapp.Constance;
import com.example.gongsidediannao.moduleapp.R;
import com.example.gongsidediannao.moduleapp.base.BaseActivity;
import com.example.gongsidediannao.moduleapp.entity.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
/**
* @Description: 
* @Author:  huyahui 
* @Date:  2022/5/5 17:44  
*/


@Route(path = Constance.ACTIVITY_AROUTER)
public class ArouterActivity extends BaseActivity {

    @Autowired(name = "content")
    String name;

    @Autowired(name = "dog")
    Animal animal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arouter);
       /* if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }*/
        if (getIntent() != null) {
            String content = getIntent().getStringExtra("content");
            ToastUtils.showToast(this, content);
        }
        setResult(3);

        findViewById(R.id.btn_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   arouterCallBack();
                Long time=System.currentTimeMillis();
                EventBus.getDefault().post(new MessageEvent(String.valueOf(time)));
            }
        });
    }

    private void arouterCallBack() {
        ARouter.getInstance().build(Constance.ACTIVITY_MAIN,Constance.GROUP_MAIN)
                .navigation(ArouterActivity.this, new NavigationCallback() {
                    @Override
                    public void onFound(Postcard postcard) {
                        // 路由目标被发现时调用
                        ToastUtils.showToast(ArouterActivity.this, "onFound");
                    }

                    @Override
                    public void onLost(Postcard postcard) {
                        //
                        ToastUtils.showToast(ArouterActivity.this, "onLost");
                    }

                    @Override
                    public void onArrival(Postcard postcard) {
                        // 路由到达后调用 一般调用这个
                        ToastUtils.showToast(ArouterActivity.this, "onArrival" + postcard.getGroup());
                    }

                    @Override
                    public void onInterrupt(Postcard postcard) {
                        ToastUtils.showToast(ArouterActivity.this, "onInterrupt");
                    }
                });
    }


    public void goMain(View view) {
        ARouter.getInstance().build(Constance.ACTIVITY_MAIN).navigation();
    }

    private static final String TAG = "ArouterActivity";
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(TAG, "onMessageEvent: ArouterActivity  "+event.getTimeStamp());
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop : ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy : ");
      /*  if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }*/
    }

}
