package com.example.gongsidediannao.moduleapp.designModel.proxy;

import android.util.Log;

/**
* @Description: 中介代理类 实现租房的接口
* @Author:  huyahui
* @Date:  2021/11/16 9:11
*/

public class IntermediaryProxy implements IRentHouse {

    private static final String TAG = "IntermediaryProxy";
    private RentHouse iRentHouse;

    public IntermediaryProxy(RentHouse iRentHouse) {
        this.iRentHouse = iRentHouse;
    }

    public IntermediaryProxy() { //调用这个方法，说明IntermediaryProxy只做RentHouse生意 透明模式
        this.iRentHouse = new RentHouse();
    }

    @Override
    public void rentHouse() {
        Log.d(TAG, "rentHouse: 中介费 4");
        iRentHouse.rentHouse();
        Log.d(TAG, "rentHouse: 中介负责维修");
    }
}
