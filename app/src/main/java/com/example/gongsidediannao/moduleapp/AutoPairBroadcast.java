package com.example.gongsidediannao.moduleapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AutoPairBroadcast extends BroadcastReceiver {

    private final String TAG = "AutoPairBroadcast";
    private BluetoothAdapter bluetoothAdapter;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null) return;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) return;
        //在Android TV 版本上无法开机接收到蓝牙开启广播，只能监听开机广播启动配对服务
        if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            enablePairService(context, true);
        }

        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            Log.d(TAG, " =================== >> ACTION:  " + action);
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_TURNING_OFF:
                    //如果蓝牙被关闭，配对服务就没存在意义了，关闭服务
                    enablePairService(context, false);
                    break;
                case BluetoothAdapter.STATE_ON:
                    //一般开机时，蓝牙会随着开机一起打开，也代表蓝牙装备就绪，那么就开启配对服务
                    //如果没随机开启，那就需要监听开机广播，然后再启动蓝牙
                    enablePairService(context, true);
                    break;
                default:
                    break;
            }
        }
        //设备挂载广播，当遥控器断开连接后，还是需要自动开启扫描。（同一遥控器多次配对就是这样）
        //加上AutoPairService.RUNNING 判断是因为配对服务如果在运行，就不要在这里进行逻辑处理，在配对服务中也有对此ACTION的处理
        else if (action.equals("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED") && !AutoPairService.RUNNING) {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            int connectState = intent.getIntExtra(BluetoothProfile.EXTRA_STATE, BluetoothProfile.STATE_CONNECTED);
            //判断一下断开的设备是不是之前连接的遥控器，通过MAC或NAME判断
       /*     if (device.getAddress().equals(AutoPairGlobalConfig.getRcMac()) || device.getName() != null
                    && (device.getName().startsWith((AutoPairGlobalConfig.getRcName())) || device.getName().startsWith(AutoPairGlobalConfig.DEF_NAME))) {
                if (connectState == BluetoothProfile.STATE_DISCONNECTED && bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
                    //断开连接，开启配对服务
                    enablePairService(context, true);
                } else if (connectState == BluetoothProfile.STATE_CONNECTED) {
                    //断开连接，停止配对服务
                    enablePairService(context, false);
                }
            }*/
        }
        // TODO　am broadcast -a android.action.hitv.STOP_LESCAN
        else if (action.equals("android.action.hitv.START_LESCAN")) {
            enablePairService(context, true);
        } else if (action.equals("android.action.hitv.STOP_LESCAN")) {
            enablePairService(context, false);
        }
    }

    public void enablePairService(Context context, boolean pair) {
        Log.d(TAG, " =================== >> enablePairService:  " + pair);
        Intent autoPairService = new Intent(context, AutoPairService.class);
        if (pair) {
            context.startService(autoPairService);
        } else {
            context.stopService(autoPairService);
        }
    }

}
