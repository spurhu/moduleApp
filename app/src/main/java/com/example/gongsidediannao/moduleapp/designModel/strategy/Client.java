package com.example.gongsidediannao.moduleapp.designModel.strategy;


public class Client {

    private static final String TAG = "Client";
    AbstractStrategy abstractStrategy;

    public void setAbstractStrategy(AbstractStrategy abstractStrategy) {
        this.abstractStrategy = abstractStrategy;
    }

    public void setPrintTravelMethod() {
        abstractStrategy.printTravelMethod();
    }

}
