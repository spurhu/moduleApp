package com.example.gongsidediannao.moduleapp;

/**
 * @作者 Huyahui
 * @主要功能  路由的路径管理
 * @创建日期 2019/8/23
 */

public final class Constance {

    public static final String TAG="app";
    public static final boolean UseInterceptor = true;
    // 页面
    public static final String ACTIVITY_AROUTER="/main/router";
    public static final String ACTIVITY_MAIN="/main/main";
    public static final String FRAGMENT_BALL="/fragment/ball";
    public static final String ACTIVITY_TEST="/main/test";


    //分组
    public static final String GROUP_MAIN="main";
    public static final String GROUP_PROJECT="project";

}
