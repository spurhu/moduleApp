package com.example.gongsidediannao.moduleapp.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app_base.utils.ToastUtils;
import com.example.gongsidediannao.moduleapp.BluetoothReceiver;
import com.example.gongsidediannao.moduleapp.R;
import com.example.gongsidediannao.moduleapp.adapter.BlueToothAdapter;
import com.example.gongsidediannao.moduleapp.utils.BlueSearchCallBck;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @Description: 蓝牙相关问题
 * @Author: huyahui
 * @Date: 2021/11/8 15:22
 */


public class BluetoothActivity extends AppCompatActivity {
    private static final String TAG = "BluetoothActivity";
    public static final int REQUEST_ENABLE_BT = 0;
    public static final int REQUEST_DISENABLE_BT = 1;

    private RecyclerView recyclerView;
    private BlueToothAdapter adapter;
    private List<BluetoothDevice> list = new ArrayList<>();
    BluetoothAdapter bluetoothAdapter;
    private Button btnPost;
    private TextView tvStatus;
    private ProgressBar progressBar;
    private Switch aSwitch;

    BluetoothReceiver bluetoothReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        Log.d(TAG, "onCreate: ");

        initView();
        openBluethooth();
        startScanBluetooth();

        //展示匹配的蓝牙设备
        showDevices();

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startScanBluetooth();
                //若已配对设备数为0，跳转到手机系统蓝牙设置界面
//                Intent enableBtIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
//                startActivity(enableBtIntent);
                Log.d(TAG, "onClick: ");
            }
        });

        bluetoothReceiver=new BluetoothReceiver();
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
        intentFilter.addAction("android.bluetooth.BluetoothAdapter.STATE_OFF");
        intentFilter.addAction("android.bluetooth.BluetoothAdapter.STATE_ON");
        intentFilter.addAction("android.bluetooth.device.action.ACL_CONNECTED");
        intentFilter.addAction("android.bluetooth.device.action.ACTION_ACL_CONNECTED");
        intentFilter.addAction("android.bluetooth.device.action.ACTION_ACL_DISCONNECTED");
        intentFilter.addAction("android.bluetooth.device.action.FOUND");
        intentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        intentFilter.addAction("android.bluetooth.device.action.NAME_CHANGED");
        intentFilter.addAction("android.bluetooth.device.action.UUID");
        intentFilter.addAction("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
        registerReceiver(bluetoothReceiver,intentFilter);

        bluetoothReceiver.setBlueSearchCallBck(new BluetoothReceiver.BlueSearchCallBck() {
            @Override
            public void onStartSearch() {
                Log.d(TAG, "onStartSearch: 666666");
            }

            @Override
            public void onSearching() {
                Log.d(TAG, "onSearching: 8888");
                btnPost.setText("onSearching");
            }

            @Override
            public void onEndSearch() {
                progressBar.setVisibility(View.GONE);
                Log.d(TAG, "onEndSearch: 9999999");
                btnPost.setText("onEndSearch");
            }

            @Override
            public void onStartBind() {
                Log.d(TAG, "onStartBind: ");
                btnPost.setText("onStartBind");
            }

            @Override
            public void onEndBind() {
                Log.d(TAG, "onEndBind: ");
                btnPost.setText("onEndBind");
            }
        });

       // BluetoothSocket

    }

    private void initView() {
        tvStatus = (TextView) findViewById(R.id.tv_status);
        btnPost = (Button) findViewById(R.id.btn_post);
        aSwitch = (Switch) findViewById(R.id.switch_ble);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        tvStatus.setText("蓝牙未打开");
        aSwitch.setChecked(false);

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Log.d(TAG, "onCheckedChanged:  打开"+b);
                    if (!bluetoothAdapter.isEnabled()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT); // 开启蓝牙
                    }
                }else{
                    Log.d(TAG, "onCheckedChanged: 关闭 "+b);
                          bluetoothAdapter.disable();
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_DISENABLE_BT); // 开启蓝牙
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }

    // 开启蓝牙
    private void openBluethooth() {
        // 检测是否支持蓝牙
        if (!isSupportBle()) {
            Log.d(TAG, "onCreate: 设备不支持蓝牙");
        } else {
            Log.d(TAG, "onCreate: 设备支持蓝牙");
            if (!bluetoothAdapter.isEnabled()) {
                aSwitch.setChecked(false);
               /* if (!bluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                     startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT); // 开启蓝牙
                }*/
            } else {
                tvStatus.setText("蓝牙已开启");
                aSwitch.setChecked(true);
                ToastUtils.showToast(BluetoothActivity.this, "蓝牙已经开启啦");
            }

        }
    }

    //是否支持蓝牙
    private boolean isSupportBle() {
        return bluetoothAdapter != null ? true : false;
    }

    /**
     * 显示匹配的蓝牙设备
     * 蓝牙配对状态
     * BOND_BONDED = 12：已配对
     * BOND_BONDING = 11：配对中
     * BOND_NONE = 10：未配对或取消配对
     */
    private void showDevices() {
        list.clear();
        //获取已配对蓝牙信息
        Set<BluetoothDevice> bluetoothDeviceSet = bluetoothAdapter.getBondedDevices();
        if (bluetoothDeviceSet != null && bluetoothDeviceSet.size() > 0) {
            for (BluetoothDevice device : bluetoothDeviceSet) {
                Log.d(TAG, "showDevices getName: " + device.getName() + " getAddress：" + device.getAddress() + " getUuids " + device.getUuids() + " 匹配状态 " + device.getBondState() + " getType  " + device.getType());
                Log.d(TAG, "showDevices describeContents: "+device.describeContents());
                Log.d(TAG, "showDevices getBluetoothClass: "+device.getBluetoothClass());
                list.add(device);
                device.connectGatt(this, true, new BluetoothGattCallback() {
                    @Override
                    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                        super.onConnectionStateChange(gatt, status, newState);
                    }
                }, 1300);
            }
        } else {
            Log.d(TAG, "showDevices: 没有已保存蓝牙设备");
        }

        //搜索可用设备
        //bluetoothAdapter.


        if (adapter == null) {
            //显示蓝牙设备
            adapter = new BlueToothAdapter(list, this);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }

        //连接设备
        adapter.setOnItemClickListener(new BlueToothAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) throws Exception {
                ToastUtils.showToast(BluetoothActivity.this, "点击位置: " + position);
                //手动配对，完成配对后重新扫描即可
                BluetoothDevice bluetoothDevice = list.get(position);
                bluetoothDevice.setPairingConfirmation(true);//开始配对
            }
        });
        
    }

    /**
     * 与设备配对 参考源码：platform/packages/apps/Settings.git
     * /Settings/src/com/android/settings/bluetooth/CachedBluetoothDevice.java
     */
    static public boolean createBond(Class btClass, BluetoothDevice btDevice)
            throws Exception {
        Log.d(TAG, "createBond: ----");
        Method createBondMethod = btClass.getMethod("createBond");
        Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }

    private void startScanBluetooth() {
        // 判断是否在搜索,如果在搜索，就取消搜索
        if (bluetoothAdapter.isDiscovering()) {
            Log.d(TAG, "startScanBluetooth: 正在搜索...");
            bluetoothAdapter.cancelDiscovery();
        }
        // 开始搜索
        bluetoothAdapter.startDiscovery();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: ");
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                tvStatus.setText("蓝牙已开启");
                Log.d(TAG, "onActivityResult: 0000000000");
                break;
            case REQUEST_DISENABLE_BT:
                tvStatus.setText("蓝牙已关闭");
                Log.d(TAG, "onActivityResult: 1111111111111");
                break;
        }
        showDevices();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        unregisterReceiver(bluetoothReceiver);
    }

}