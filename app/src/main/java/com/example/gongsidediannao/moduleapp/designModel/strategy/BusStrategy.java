package com.example.gongsidediannao.moduleapp.designModel.strategy;

import android.util.Log;

public class BusStrategy implements AbstractStrategy {

    private static final String TAG = "BusStrategy";

    @Override
    public void printTravelMethod() {
        Log.d(TAG, "printTravelMethod: 汽车");
    }
}
