package com.example.gongsidediannao.moduleapp.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gongsidediannao.moduleapp.R;

import java.util.List;

import static android.bluetooth.BluetoothDevice.BOND_BONDED;
import static android.bluetooth.BluetoothDevice.BOND_BONDING;
import static android.bluetooth.BluetoothDevice.BOND_NONE;

public class BlueToothAdapter extends RecyclerView.Adapter<BlueToothAdapter.BlueToothHolderView> {

    private List<BluetoothDevice> bluetoothDeviceList;
    private Context context;

    public BlueToothAdapter(List<BluetoothDevice> bluetoothDeviceList, Context context) {
        this.bluetoothDeviceList = bluetoothDeviceList;
        this.context = context;
    }

    public interface OnItemClickListener {
        void onItemClick(int position) throws Exception;
    }

    OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public BlueToothHolderView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bluetooth, null);
        return new BlueToothHolderView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BlueToothHolderView holder, int position) {

        BluetoothDevice device = bluetoothDeviceList.get(position);
        String status = null;
        switch (device.getBondState()) {
            case BOND_BONDED:
                status = " 已配对";
                break;
            case BOND_BONDING:
                status = " 正在配对..";
                break;
            case BOND_NONE:
                status = " 未配对或取消配对";
                break;
        }
        holder.tvStatus.setText(status);
        holder.tvName.setText("名称: " + device.getName());
        holder.tvAddress.setText("地址: " + device.getAddress());
        if (holder != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        onItemClickListener.onItemClick(position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return bluetoothDeviceList == null ? 0 : bluetoothDeviceList.size();
    }

    public class BlueToothHolderView extends RecyclerView.ViewHolder {
        private TextView tvName, tvAddress,tvStatus;

        public BlueToothHolderView(@NonNull View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.textView);
            tvAddress = (TextView) itemView.findViewById(R.id.textView2);
            tvStatus = (TextView) itemView.findViewById(R.id.textView3);
        }
    }
}
