package com.example.gongsidediannao.moduleapp.utils;

public interface BlueSearchCallBck {

    void onStartSearch(); //开始搜索
    void onSearching(); //搜索中
    void onEndSearch(); //搜索完成
}
