package com.example.gongsidediannao.moduleapp.designModel.proxy;

/**
* @Description: 租房方法接口
* @Author:  huyahui
* @Date:  2021/11/16 9:03  
*/


public interface IRentHouse {
    
    void rentHouse();
}
