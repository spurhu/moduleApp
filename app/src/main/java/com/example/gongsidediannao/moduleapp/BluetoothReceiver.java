package com.example.gongsidediannao.moduleapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.app_base.utils.ToastUtils;
import static android.bluetooth.BluetoothDevice.ACTION_NAME_CHANGED;
import static android.bluetooth.BluetoothDevice.BOND_BONDED;
import static android.bluetooth.BluetoothDevice.BOND_BONDING;
import static android.bluetooth.BluetoothDevice.BOND_NONE;
import static android.bluetooth.BluetoothDevice.EXTRA_DEVICE;
import static android.bluetooth.BluetoothDevice.EXTRA_PAIRING_KEY;
import static android.bluetooth.BluetoothDevice.EXTRA_PAIRING_VARIANT;
import static com.example.gongsidediannao.moduleapp.AutoPairService.ACTION_UUID;
/**
* @Description:
 *
 * BluetoothDevice api说明:https://blog.csdn.net/xfxf996/article/details/82814081
* @Author:  huyahui
* @Date:  2021/11/10 10:33
*/


public class BluetoothReceiver extends BroadcastReceiver {

    private static final String TAG = "BluetoothActivity";
    String content = "";
    String content2 = "";

    public interface BlueSearchCallBck {
        void onStartSearch(); //开始搜索
        void onSearching(); //搜索中
        void onEndSearch(); //搜索完成

        void onStartBind(); //开始绑定
        void onEndBind(); //绑定完成
    }

    BlueSearchCallBck blueSearchCallBck;

    public void setBlueSearchCallBck(BlueSearchCallBck blueSearchCallBck) {
        this.blueSearchCallBck = blueSearchCallBck;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            // 蓝牙配对状态改变广播  绑定也叫配对
            case BluetoothDevice.ACTION_BOND_STATE_CHANGED: // 表示远程设备的绑定状态发生变化
                int bondState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                Log.d(TAG, "onReceive bondState : " + bondState);
                if (bondState == BOND_NONE) { // 绑定失败
                    Log.d(TAG, "onReceive: 绑定失败或解除绑定 " + millTime());
                    content2 = "绑定失败";
                } else if (bondState == BOND_BONDING) { // 正在绑定...
                    Log.d(TAG, "onReceive: 正在绑定... " + millTime());
                    blueSearchCallBck.onStartBind();
                    content2 = "正在绑定";
                } else if (bondState == BOND_BONDED) { // 绑定成功
                    Log.d(TAG, "onReceive: 绑定成功 " + millTime());
                    blueSearchCallBck.onEndBind();
                    content2 = "绑定成功";
                } else if (bondState == BluetoothDevice.ERROR) {
                    Log.d(TAG, "onReceive: 绑定异常 " + millTime());
                    content2 = "绑定异常";
                }
                ToastUtils.showToast(context, content2);
                break;
            case BluetoothDevice.ACTION_PAIRING_REQUEST: //用于广播配对请求
                Log.d(TAG, "onReceive: PAIRING....... " + millTime());
                int state = intent.getIntExtra(EXTRA_PAIRING_KEY, BluetoothDevice.ERROR);
                int state2 = intent.getIntExtra(EXTRA_PAIRING_VARIANT, BluetoothDevice.ERROR);
                Log.d(TAG, "onReceive: PAIRING....state " + state); //PIN码
                Log.d(TAG, "onReceive: PAIRING....state2 " + state2); // PAIRING_VARIANT_PASSKEY_CONFIRMATION:2
                // https://cloud.tencent.com/developer/article/1436704
                switch (state) {
                    case BOND_BONDED: // 12
                        Log.d(TAG, "onReceive: 已配对");
                        break;
                    case BOND_BONDING: // 11
                        Log.d(TAG, "onReceive: 配对中");
                        break;
                    case BOND_NONE: // 10
                        Log.d(TAG, "onReceive: 未配对或取消配对");
                        break;

                }
                Log.d(TAG, "onReceive state : " + state);//PIN码
                break;
            case BluetoothAdapter.ACTION_STATE_CHANGED:
                Log.d(TAG, "onReceive ACTION_STATE_CHANGED :"+ intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0));
                switch (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0)) {
                    case BluetoothAdapter.STATE_TURNING_ON:
                        content = "STATE_TURNING_ON 蓝牙开启中";
                        break;
                    case BluetoothAdapter.STATE_ON:
                        content = " STATE_ON 蓝牙开启";
                        break;
                    case BluetoothAdapter.STATE_CONNECTING:
                        content = " STATE_CONNECTING 蓝牙连接中";
                        break;
                    case BluetoothAdapter.STATE_CONNECTED:
                        content = " STATE_CONNECTED 蓝牙已连接";
                        break;
                    case BluetoothAdapter.STATE_DISCONNECTING:
                        content = "STATE_DISCONNECTING 蓝牙断开中 ";
                        break;
                    case BluetoothAdapter.STATE_DISCONNECTED:
                        content = " STATE_DISCONNECTED 蓝牙已断开";
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        content = " STATE_TURNING_OFF 蓝牙关闭中";
                        break;
                    case BluetoothAdapter.STATE_OFF:
                        content = "STATE_OFF 蓝牙关闭 ";
                        break;
                }
                Log.d(TAG, "onReceive: // " + content);
                break;
            case BluetoothDevice.ACTION_ACL_CONNECTED: // 表示已与远程设备建立低级 (ACL) 连接
                // 蓝牙已连接
                Log.d(TAG, "onReceive: // 蓝牙已连接: " + millTime());
                break;
            case BluetoothDevice.ACTION_ACL_DISCONNECTED:// 表示与远程设备的低级别 (ACL) 断开连接
                // 蓝牙已断开
                Log.d(TAG, "onReceive: / 蓝牙已断开: " + millTime());
                break;
            case BluetoothDevice.ACTION_FOUND:

                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //蓝牙rssi参数，代表蓝牙强度
                short rssi = intent.getExtras().getShort(BluetoothDevice.EXTRA_RSSI);
                //蓝牙设备名称
                String name = device.getName();
                //蓝牙设备连接状态
                int status = device.getBondState();
                Log.d(TAG, "onReceive: 发现远程设备" + rssi + " name: " + name + " status: " + status);
                break;
            case ACTION_NAME_CHANGED: // 指示远程设备的友好名称已被首次检索，或自上次检索以来已更改
                Log.d(TAG, "onReceive: 远程设备的友好名称已被首次检索");
                BluetoothDevice bluetoothDevice = intent.getParcelableExtra(EXTRA_DEVICE);
                Log.d(TAG, "onReceive getName: " + bluetoothDevice.getName() + "  getAddress:" + bluetoothDevice.getAddress());
                break;
            case BluetoothDevice.ACTION_UUID: // 用于在获取远程设备后广播UUID 包装为ParcelUuid远程设备的
                String uuid = intent.getStringExtra(ACTION_UUID);
                Log.d(TAG, "onReceive: ACTION_UUID:" + uuid);
                break;
            case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                blueSearchCallBck.onEndSearch();
                Log.d(TAG, "onReceive: 蓝牙设备搜索完成");
                Toast.makeText(context, "onReceive 蓝牙设备搜索完成", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    private Long millTime() {
        return System.currentTimeMillis() / 1000;
    }
}


