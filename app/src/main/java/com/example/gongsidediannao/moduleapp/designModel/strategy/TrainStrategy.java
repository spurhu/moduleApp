package com.example.gongsidediannao.moduleapp.designModel.strategy;

import android.util.Log;

public class TrainStrategy implements AbstractStrategy {

    private static final String TAG = "TrainStrategy";

    @Override
    public void printTravelMethod() {
        Log.d(TAG, "printTravelMethod: 坐高铁");
    }
}
