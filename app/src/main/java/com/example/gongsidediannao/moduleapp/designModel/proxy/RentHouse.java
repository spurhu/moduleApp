package com.example.gongsidediannao.moduleapp.designModel.proxy;

import android.util.Log;

/**
* @Description: 租房方法实现类 代理模式
 *  https://zhuanlan.zhihu.com/p/72644638
 *  https://www.zhihu.com/search?type=content&q=%E4%BB%A3%E7%90%86%E6%A8%A1%E5%BC%8F
 *
* @Author:  huyahui 
* @Date:  2021/11/16 9:04
 * 为其他对象提供一种代理以控制对这个对象的访问
*/


public class RentHouse implements IRentHouse {

    private static final String TAG = "RentHouse";
    @Override
    public void rentHouse() {
        Log.d(TAG, "rentHouse: 租房");
    }
}
