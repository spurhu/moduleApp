package com.example.gongsidediannao.moduleapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtil {

    public SharedPreferences sharedPreferences = null;
    public SharedPreferences.Editor editor;
    public static final String SP_KEY_NAME = "name";
    private static SharedPreferenceUtil sharedPreferenceUtil;

    public static SharedPreferenceUtil getInstance() {
        if (sharedPreferenceUtil == null) {
            sharedPreferenceUtil = new SharedPreferenceUtil();
        }
        return sharedPreferenceUtil;
    }

    public void init(Context context) {
        sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void putValue(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }


    public String getValue(String key, String defaultValue) {
        String value = sharedPreferences.getString(key, defaultValue);
        return value;
    }

}
