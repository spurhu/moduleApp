package com.example.gongsidediannao.moduleapp.designModel.strategy;

/**
* @Description:  策略模式抽象接口
* @Author:  huyahui
* @Date:  2021/12/15 9:37
* 定义了一组策略，分别在不同类中封装起来，每种策略都可以根据当前场景相互替换，从而使策略的变化可以独立于操作者
* 比如我们要去某个地方，会根据距离的不同（或者是根据手头经济状况）来选择不同的出行方式（共享单车、坐公交、滴滴打车等等），这些出行方式即不同的策略。
*/


public interface AbstractStrategy {

    // 打印出来旅行的方法
    void printTravelMethod();
}
