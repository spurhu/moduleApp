package com.example.gongsidediannao.moduleapp.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.example.gongsidediannao.moduleapp.BuildConfig;
import com.example.gongsidediannao.moduleapp.R;
import com.example.gongsidediannao.moduleapp.base.BaseActivity;
import com.example.gongsidediannao.moduleapp.designModel.strategy.AbstractStrategy;
import com.example.gongsidediannao.moduleapp.designModel.strategy.BusStrategy;
import com.example.gongsidediannao.moduleapp.designModel.strategy.Client;
import com.example.gongsidediannao.moduleapp.designModel.strategy.TrainStrategy;
import com.example.gongsidediannao.moduleapp.entity.AppInfoBean;
import com.example.gongsidediannao.moduleapp.entity.MessageEvent;
import com.example.gongsidediannao.moduleapp.designModel.proxy.IntermediaryProxy;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.os.SystemProperties;

import static com.example.gongsidediannao.moduleapp.Constance.ACTIVITY_TEST;

@Route(path = ACTIVITY_TEST)
public class TestActivity extends BaseActivity {
    private static final String TAG = "TestActivity";
    public static final String packageName = "com.android.tv.launcherapp";
    public static final int REQUEST_ENABLE_BT = 0;
    public static final int REQUEST_DISENABLE_BT = 1;

    public static final int MSG_INFO = 0;
    private Button btnPost;

    RelativeLayout.LayoutParams layoutParams = null;

    // Only the original thread that created a view hierarchy can touch its views.
    // 只有创建视图层次结构的原始线程才能访问它的视图。

    Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_INFO:
                    Log.d(TAG, "handleMessage: ");
                    layoutParams.height = 400;
                    btnPost.setLayoutParams(layoutParams);
                    break;
            }
        }
    };

    public static void initProcess2() {
        try {
            Runtime.getRuntime().exec("su");
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", "initProcess getMessage: " + e.getMessage());
            Log.d("TAG", "initProcess getCause : " + e.getCause());
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        initView();
        // getAutoApps();

        // portReset();

        //租房--代理模式
       /* RentHouse rentHouse=new RentHouse();
        IntermediaryProxy intermediaryProxy=new IntermediaryProxy(rentHouse);
        intermediaryProxy.rentHouse();*/

        IntermediaryProxy intermediaryProxy = new IntermediaryProxy();
        intermediaryProxy.rentHouse();

        //策略模式
        Client client = new Client();
        AbstractStrategy busStrategy = new BusStrategy();
        AbstractStrategy trainStrategy = new TrainStrategy();

        client.setAbstractStrategy(busStrategy);
        client.setPrintTravelMethod();

        client.setAbstractStrategy(trainStrategy);
        client.setPrintTravelMethod();
    }

    private static java.lang.Process process;

    /**
     * 端口重定向
     */
    public static void portReset() {
        Log.d("TAG", "portReset: ");
        initProcess();
        OutputStream outputStream = process.getOutputStream();
        String cmd = "iptables -t nat -A PREROUTING -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 1025 ";
        try {
            outputStream.write(cmd.getBytes());
            outputStream.flush();
            //close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void initProcess() {
        if (process == null) {
            try {
                process = Runtime.getRuntime().exec("su");
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("TAG", "initProcess getMessage: " + e.getMessage());
                Log.d("TAG", "initProcess getCause : " + e.getCause());
            }
        }
    }

    private void getAutoApps() {
        List<AppInfoBean> array = new ArrayList<AppInfoBean>();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);  //查询全部带Launcher 启动项的app 列表
        // 通过查询，获得所有ResolveInfo对象.
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> resolveInfos = getPackageManager().queryIntentActivities(mainIntent, 0);
        Log.d(TAG, "getAutoApps: " + resolveInfos.size());
        for (ResolveInfo reInfo : resolveInfos) {

            if (BuildConfig.APPLICATION_ID.equals(reInfo.activityInfo.packageName))
                continue;  //过滤掉 自己本身
            AppInfoBean appinfo = new AppInfoBean();
            appinfo.activityName = reInfo.activityInfo.name; // 获得该应用程序的启动Activity的name
            appinfo.packageName = reInfo.activityInfo.packageName; // 获得应用程序的包名
            appinfo.appLabel = (String) reInfo.loadLabel(packageManager); // 获得应用程序的Label
            // 为应用程序的启动Activity 准备Intent
            array.add(appinfo);
            Log.d(TAG, "getAutoApps name : " + reInfo.activityInfo.name);
        }

    }

    private void initView() {
        btnPost = (Button) findViewById(R.id.btn_post);
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHardDeviceInfo();
            }
        });
        if (Looper.myLooper() == Looper.getMainLooper()) {
            Log.d(TAG, "onClick: main");
        } else {
            Log.d(TAG, "onClick: zixiancheng");
        }
        Log.d(TAG, "run 1: " + android.os.Process.myPid());


        new MyThread().start();

     /*   bindService(new Intent("cc.abto.server"), new ServiceConnection()
        {

            @Override
            public void onServiceConnected(ComponentName name, IBinder service)
            {

              //  iMyAidlInterface = IMyAidlInterface.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name)
            {

            }
        }, BIND_AUTO_CREATE);*/
    }

    private void getHardDeviceInfo() {
        Log.d(TAG, "getHardDeviceInfo: " + SystemProperties.get("ro.build.date.utc"));
        Log.d(TAG, "getHardDeviceInfo: " + SystemProperties.get("ro.build.id"));

        StringBuffer sb = new StringBuffer();
        sb.append("ro.build.id" + " : " + Build.ID + "\n");
        sb.append("ro.build.display.id" + " : " + Build.DISPLAY + "\n");
        sb.append("ro.product.name" + " : " + Build.PRODUCT + "\n");
        sb.append("ro.product.device" + " : " + Build.DEVICE + "\n");
        sb.append("ro.product.board" + " : " + Build.BOARD + "\n");
        sb.append("ro.product.cpu.abi" + " : " + Build.CPU_ABI + "\n");
        sb.append("ro.product.cpu.abi2" + " : " + Build.CPU_ABI2 + "\n");
        sb.append("ro.product.manufacturer" + " : " + Build.MANUFACTURER + "\n");
        sb.append("ro.product.brand" + " : " + Build.BRAND + "\n");
        sb.append("ro.product.model" + " : " + Build.MODEL + "\n");
        sb.append("ro.bootloader" + " : " + Build.BOOTLOADER + "\n");

        sb.append("ro.hardware" + " : " + Build.HARDWARE + "\n");
        sb.append("ro.serialno" + " : " + Build.SERIAL + "\n");
        sb.append("ro.build.type" + " : " + Build.TYPE + "\n");
        sb.append("ro.build.tags" + " : " + Build.TAGS + "\n");
        sb.append("ro.build.fingerprint" + " : " + Build.FINGERPRINT + "\n");
        sb.append("ro.build.date.utc" + " : " + Build.TIME + "\n");
        sb.append("ro.build.user" + " : " + Build.USER + "\n");
        sb.append("ro.build.host" + " : " + Build.HOST + "\n");
        sb.append("radioVersion" + " : " + Build.getRadioVersion() + "\n");

        sb.append("ro.build.version.incremental" + " : " + Build.VERSION.INCREMENTAL + "\n");
        sb.append("ro.build.version.release" + " : " + Build.VERSION.RELEASE + "\n");
        sb.append("ro.build.version.sdk" + " : " + Build.VERSION.SDK_INT + "\n");
        sb.append("ro.build.version.codename" + " : " + Build.VERSION.CODENAME + "\n");


    }


    public class MyThread extends Thread {
        @Override
        public void run() {
            super.run();

        }
    }

    /**
     * 没用
     */
    private void deleteApp() {
        // 卸载app 不要权限
        Uri uri = Uri.fromParts("package", packageName, null);
        Intent intent = new Intent(Intent.ACTION_DELETE, uri);
        //startActivity(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(TAG, "onMessageEvent: TestActivity  " + event.getTimeStamp());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }
}