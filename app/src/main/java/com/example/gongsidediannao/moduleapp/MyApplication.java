package com.example.gongsidediannao.moduleapp;

import com.alibaba.android.arouter.launcher.ARouter;
import com.example.gongsidediannao.moduleapp.utils.SharedPreferenceUtil;

import android.app.Application;

/**
 * @作者 Huyahui
 * @主要功能
 * @创建日期 2019/8/23
 */

public class MyApplication extends Application {

    private boolean isDebugARouter = true;

    @Override
    public void onCreate() {
        super.onCreate();
        if(isDebugARouter){
            ARouter.openLog();
            ARouter.openDebug(); // 开启调试模式
        }
        ARouter.init(MyApplication.this);
        SharedPreferenceUtil.getInstance().init(this);
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        ARouter.getInstance().destroy(); // 解绑
    }


}
