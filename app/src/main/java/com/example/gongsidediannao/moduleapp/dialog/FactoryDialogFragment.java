package com.example.gongsidediannao.moduleapp.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.gongsidediannao.moduleapp.R;

public class FactoryDialogFragment extends DialogFragment {
    private static final String TAG = "FactoryDialogFragment";

    private Button btn_sure;
    private Button btn_cancel;

    private int DEFAULT_WIDTH = WindowManager.LayoutParams.WRAP_CONTENT;//宽
    private int DEFAULT_HEIGHT = WindowManager.LayoutParams.WRAP_CONTENT;//高
    private int DEFAULT_GRAVITY = Gravity.CENTER;//位置

    private boolean mCancelable = true;//默认可取消
    private boolean mCanceledOnTouchOutside = false;//默认点击外部可取消

    static FactoryDialogFragment factoryDialogFragment;
    public static FactoryDialogFragment getInstance(){
        if (factoryDialogFragment == null) {
            factoryDialogFragment=new FactoryDialogFragment();
        }
        return factoryDialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.dialog_factory,null);
        btn_sure=(Button)view.findViewById(R.id.btn_ok);
        btn_cancel=(Button)view.findViewById(R.id.btn_cancel);
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "onClick: 恢复出厂设置");
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
                Log.d(TAG, "onClick: 取消");
            }
        });
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog mDialog = super.onCreateDialog(savedInstanceState);
        if (null != mDialog) {//初始化
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.setCanceledOnTouchOutside(mCanceledOnTouchOutside);
            mDialog.setCancelable(mCancelable);
            Window window = mDialog.getWindow();
            if (null != window) {
                window.getDecorView().setPadding(0, 0, 0, 0);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                WindowManager.LayoutParams lp = window.getAttributes();
                lp.width = DEFAULT_WIDTH;
                lp.height = DEFAULT_HEIGHT;
                lp.gravity = DEFAULT_GRAVITY;
                lp.windowAnimations = android.R.style.Animation_InputMethod;
                window.setAttributes(lp);
            }
            mDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    return !mCancelable;
                }
            });
        }
        return mDialog;
    }



}
