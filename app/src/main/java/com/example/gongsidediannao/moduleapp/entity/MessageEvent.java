package com.example.gongsidediannao.moduleapp.entity;

public class MessageEvent {

    private String timeStamp;

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public MessageEvent() {
    }

    public MessageEvent(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
