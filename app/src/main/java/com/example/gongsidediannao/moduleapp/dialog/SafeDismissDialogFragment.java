package com.example.gongsidediannao.moduleapp.dialog;

import android.app.Activity;
import android.app.DialogFragment;

public class SafeDismissDialogFragment extends DialogFragment {
    private boolean mAttached = false;
    private boolean mDismissPending = false;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mAttached = true;
        if (mDismissPending) {
            mDismissPending = false;
            dismiss();
            return;
        }
    }

    @Override
    public void onDetach() {
        mAttached = false;
        super.onDetach();
    }

    /**
     * Dismiss safely regardless of the DialogFragment's life cycle.
     */
    @Override
    public void dismiss() {
        if (!mAttached) {
            mDismissPending = true;
        } else {
            super.dismiss();
        }
    }
}
