package com.example.gongsidediannao.moduleapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.gongsidediannao.moduleapp.activity.MainActivity;
import com.example.gongsidediannao.moduleapp.activity.TestActivity;

public class BootBroadcastReceiver extends BroadcastReceiver {

    static  final  String ACTION =  "android.intent.action.BOOT_COMPLETED" ;
    private static final String TAG = "BootBroadcastReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        if  (intent.getAction().equals(ACTION)) {
            Log.d(TAG, "onReceive: 开机啦");
            Toast.makeText(context,"开机啦",Toast.LENGTH_SHORT).show();
            Intent mainActivityIntent =  new  Intent(context, MainActivity.class );   // 要启动的Activity
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mainActivityIntent);
        }
    }
}
