package com.example.gongsidediannao.moduleapp.entity;

/**
* @Description:
* @Author:  huyahui
* @Date:  2021/11/8 13:39
*/


public class BlueToothInfo {

    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BlueToothInfo(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
