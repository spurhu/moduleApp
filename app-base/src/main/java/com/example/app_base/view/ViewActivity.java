package com.example.app_base.view;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.example.app_base.R;

public class ViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
    }
}
